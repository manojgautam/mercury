<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


class Cl_Page_Builder{
    
    public $post_shortcodes = array();
    private $post_content = '';
    private $page_header = '';
    public $content_blocks = array();
    private $tag_index = 0;
    public function init(){
        
        if(is_customize_preview()){

        	add_action( 'the_post', array(&$this, 'parseEditableContent', ), 9999 ); // after all the_post actions ended
	    	add_action( 'wp_footer', array( &$this, 'printPostShortcodes', ) );
	    	add_action( 'customize_register', array(&$this, 'register_content_setting') );

	    	add_action( 'wp_head', array(&$this, 'add_content_ui') );
        
        }
        
        
    	add_action( 'cl_ajax_handler_cl_load_shortcode', array(&$this, 'load_shortcode_ajax'), 9999);
		add_action( 'wp_ajax_cl_save_page_content', array(&$this, 'cl_save_page_content'));
		add_action( 'wp_ajax_cl_save_template', array(&$this, 'cl_save_template'));
    }


    public function cl_save_template(){
    	$data = $_POST['data'];
    	

    	$my_post = array(
		      //'post_title'   => 'Updated',
		      'post_content' => $data['content'],
		      'post_title' => $data['name'],
		      'post_type' => 'content_block',
		      'post_status' => 'publish'
		);
		
		wp_insert_post($my_post);	
    	
    	wp_send_json_success();
    }

    public function add_content_ui(){
    	
    	if( codeless_get_post_id() == 0 )
    		return;

    	if( is_single() && codeless_get_post_style() == 'custom' ){
    		// Post Content -> Begin
			add_action( 'codeless_hook_post_content_begin', array(&$this, 'prepend_element_html' ) );
			// Post Content -> End
			add_action( 'codeless_hook_post_content_end', array(&$this, 'append_element_html' ) );

    	}else{

			if( ! codeless_is_inner_content() ){
				   // Content -> Begin
				add_action( 'codeless_hook_content_begin', array(&$this, 'prepend_element_html' ) );
				// Content -> End
				add_action( 'codeless_hook_content_end', array(&$this, 'append_element_html' ) );

			}else{
				// Content -> Begin
				add_action( 'codeless_hook_content_column_begin', array(&$this, 'prepend_element_html' ) );
				// Content -> End
				add_action( 'codeless_hook_content_column_end', array(&$this, 'append_element_html' ) );
			}
    	}

    	
    }
    
    public function force_empty_post_dates( $data ) {
		$empty_date = '0000-00-00 00:00:00';
		$date_fields = array(
			'post_date',
			'post_date_gmt',
			'post_modified',
			'post_modified_gmt',
		);
		$data = array_merge(
			$data,
			wp_slash( array_fill_keys( $date_fields, $empty_date ) )
		);
		return $data;
	}
    
    public function cl_save_page_content(){
    	$data = $_POST['data'];
    	
    	foreach($data as $post_id => $content){
    		
    		if( $post_id == 'changeset' )
    			continue;

    		$my_post = array(
		      'ID'           => $post_id,
		      //'post_title'   => 'Updated',
		      'post_content' => $content,
			);
			$post = wp_update_post($my_post);
    	}
    	
    	
    	wp_send_json_success();
    }
    
    
    public function register_content_setting($wp_customize){
    	
		$wp_customize->add_setting( 'cl_content_settings_updated' , array(
		    'default' => '',
		    'transport' => 'postMessage'
		) );
		
		$wp_customize->add_setting( 'cl_page_content' , array(
		    'default' => '',
		    'transport' => 'postMessage'
		) );

		$wp_customize->add_setting( 'cl-style-clipboard' , array(
		    'default' => '',
		    'transport' => 'postMessage'
		) );

		$wp_customize->add_setting( 'cl-element-clipboard' , array(
		    'default' => '',
		    'transport' => 'postMessage'
		) );
		
			
		//return $wp_customize;
    }
    
    public function addToMapper($attrs){
        Cl_Builder_Mapper::map($attrs['tag'], $attrs);
    }
    
    
    public function parseEditableContent( $post ) {
    	if($post->ID != codeless_get_post_id() )
	    	$post = get_post( codeless_get_post_id() );
	    

    	if( $post->post_type == 'post' && codeless_get_post_style() != 'custom' )
    		return false;

		if ( (int) $post->ID > 0 && ! defined( 'CL_LOADING_EDITABLE_CONTENT' )) {
			


			define( 'CL_LOADING_EDITABLE_CONTENT', 1 );
			remove_filter( 'the_content', 'wpautop' );
		
			ob_start();
			if( is_customize_preview() )
				$this->buildContentBlocks();

			$page_header    = codeless_extract_page_header($post->post_content);
			$content = $post->post_content;

			if( ( codeless_get_page_layout() != 'fullwidth'  || 
				  codeless_is_blog_query() || 
				  ( is_single() && 
				  	get_post_type(codeless_get_post_id()) == 'post' && 
				  	codeless_get_post_style() == 'custom' 
				  ) 
				 ) &&
				 ! codeless_is_shop_page()  )
				$content = str_replace($page_header, '', $content);

			$this->getPageShortcodesByContent( $content );
			
			if( is_customize_preview() )
				$this->buildPredefinedList();
			$this->post_content = ob_get_clean();

			ob_start();
			

			if( ( codeless_get_page_layout() != 'fullwidth'  || 
				  codeless_is_blog_query() || 
				  ( is_single() && 
				  	get_post_type(codeless_get_post_id()) == 'post' && 
				  	codeless_get_post_style() == 'custom' 
				  ) 
				) &&
				! codeless_is_shop_page()  )
				
				$this->page_header .= $this->parseShortcodesString($page_header);

			if(is_customize_preview()){
				cl_include_template( 'post_shortcodes.tpl.php', array( 'cl_page_builder' => $this ) );
				cl_include_template( 'dialog.tpl.php', array( 'cl_page_builder' => $this ) );
				cl_include_template( 'cl_row-video.tpl.php', array( 'cl_page_builder' => $this ) );
			}
			$post_shortcodes = ob_get_clean();
			
			add_filter('codeless_the_content', array($this, 'editableContent'));
			
			add_filter('codeless_the_page_header', array($this, 'pageHeaderContent'));


			//add_filter('the_content', array($this, 'editableContent'));
			wp_reset_postdata();
			wp_reset_query();
			$GLOBALS['cl_post_content'] =  $post_shortcodes;
			
		}
	}
    
    public function pageHeaderContent(){
    	return $this->page_header;
    }

	public function buildPredefinedList(){
		foreach ( Cl_Builder_Mapper::getShortcodes() as $tag => $attrs ) {
			if ( isset( $attrs['predefined']) ) {

				foreach($attrs['predefined'] as $pre_id => $pre){
					$content = $pre['content'];
					preg_match_all( '/' . self::shortcodesRegexp() . '/', trim( $content ), $found );
					
					if ( count( $found[2] ) === 0 ) {
						return $is_container && strlen( $content ) > 0 ? $this->parseShortcodesString( '[cl_text]' . $content . '[/cl_text]', false, false ) : $content;
					}
					foreach ( $found[2] as $index => $s ) {
						$id = md5( time() . '-' . $this->tag_index ++ );
						$content = $found[5][ $index ];
						$shortcode = array(
							'tag' => $s,
							'attrs_query' => $found[3][ $index ],
							'attrs' => shortcode_parse_atts( $found[3][ $index ] ),
							'id' => $id,
							'parent_id' => false,
						);
						if ( false !== Cl_Builder_Mapper::getParam( $s, 'content' ) ) {
							$shortcode['attrs']['content'] = $content;
						}

						Cl_Builder_Mapper::editPredefined( $tag, $pre_id, rawurlencode( json_encode( $shortcode ) ) );
						
					}
				}
			}
		}
	}

	public function buildContentBlocks(){
		$content_blocks = get_posts( array('post_type' => 'content_block', 'posts_per_page' => -1, 'order' => 'asc') );
		if ( ! empty( $content_blocks ) && ! is_wp_error( $content_blocks ) ){
			foreach($content_blocks as $block){
				$this->content_blocks[$block->ID] = array( 'id' => $block->ID, 'name' => $block->post_title, 'content' => array() );

				$this->buildContentBlock($block->ID, $block->post_content);
			}
		}
	}

	public function buildContentBlock($content_id, $content, $is_container = false, $parent_id = false){
		
		preg_match_all( '/' . self::shortcodesRegexp() . '/', trim( $content ), $found );
					
		if ( count( $found[2] ) === 0 ) {
		return $is_container && strlen( $content ) > 0 ? $this->parseShortcodesString($content_id,  '[cl_text]' . $content . '[/cl_text]', false, $parent_id ) : $content;
					}
		$ii = 0;  
		foreach ( $found[2] as $index => $s ) {
			$ii++;
			$id = md5( time() . '-' . $this->tag_index ++ );
			$content = $found[5][ $index ];
			$shortcode = array(
							'tag' => $s,
							'attrs_query' => $found[3][ $index ],
							'attrs' => shortcode_parse_atts( $found[3][ $index ] ),
							'id' => $id,
							'parent_id' => $parent_id,
			);

			if( $ii == 1 && $parent_id == false && $s == 'cl_row' )
				$this->content_blocks[$content_id]['type'] = 'cl_row';

			if( $ii == 1 && $parent_id == false && $s == 'cl_column' )
				$this->content_blocks[$content_id]['type'] = 'cl_column';

			if ( false !== Cl_Builder_Mapper::getParam( $s, 'content' ) ) {
				$shortcode['attrs']['content'] = $content;
			}

			$this->content_blocks[$content_id]['content'][] = rawurlencode( json_encode( $shortcode ) );
			
			$shortcode_obj = $this->getShortCode( $shortcode['tag'] );
			$is_container = $shortcode_obj->settings( 'is_container' ) || ( null !== $shortcode_obj->settings( 'as_parent' ) && false !== $shortcode_obj->settings( 'as_parent' ) );
			
			if( $is_container )
				$this->buildContentBlock( $content_id, $content, $is_container, $shortcode['id'] ) ;		
		}
	}

    
    public function editableContent( $content ) {
		
		return $this->post_content;
	}
    
    
    public function printPostShortcodes() {
        
		echo isset( $GLOBALS['cl_post_content'] ) ? $GLOBALS['cl_post_content'] : '';
	}
    
    
    public static function shortcodesRegexp() {
		$tagnames = array_keys( Cl_Builder_Mapper::getShortcodes() );
		return get_shortcode_regex( $tagnames );

	}
	
	
    
    function getPageShortcodesByContent( $content ) {
		if ( ! empty( $this->post_shortcodes ) ) {
			return;
		}
		
		$cl_page_content = codeless_get_mod('cl_page_content');

		if( isset($_GET['customize_changeset_uuid']) && isset($cl_page_content['changeset']) && $_GET['customize_changeset_uuid'] == $cl_page_content['changeset'] && isset( $cl_page_content[ codeless_get_post_id() ] ) )
			$content = $cl_page_content[ codeless_get_post_id() ];

		//echo $content;
		
		$content = shortcode_unautop( trim( $content ) );
	
		$not_shortcodes = preg_split( '/' . self::shortcodesRegexp() . '/', $content );
		
		foreach ( $not_shortcodes as $string ) {
			$temp = str_replace( array(
				'<p>',
				'</p>',
			), '', $string );
			if ( strlen( trim( $temp ) ) > 0  ) {
				$content = preg_replace( '/(' . preg_quote( $string, '/' ) . '(?!\[\/))/', '[cl_row][cl_column width="1/1"][cl_text]$1[/cl_text][/cl_column][/cl_row]', $content );
			}
		}

		echo $this->parseShortcodesString( $content );
	}
	
	

	/**
	 * @param $content
	 * @param bool $is_container
	 * @param bool $parent_id
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function parseShortcodesString( $content, $is_container = false, $parent_id = false) {
		$string = '';
		preg_match_all( '/' . self::shortcodesRegexp() . '/', trim( $content ), $found );
		
		Cl_Builder_Mapper::addShortcodes();


		if ( count( $found[2] ) === 0 ) {
			return $is_container && strlen( $content ) > 0 ? $this->parseShortcodesString( '[cl_text]' . $content . '[/cl_text]', false, $parent_id ) : $content;
		}
		foreach ( $found[2] as $index => $s ) {
			$id = md5( time() . '-' . $this->tag_index ++ );
			$content = $found[5][ $index ];
			$shortcode = array(
				'tag' => $s,
				'attrs_query' => $found[3][ $index ],
				'attrs' => shortcode_parse_atts( $found[3][ $index ] ),
				'id' => $id,
				'parent_id' => $parent_id,
			);
			if ( false !== Cl_Builder_Mapper::getParam( $s, 'content' ) ) {
				$shortcode['attrs']['content'] = $content;
			}

			$this->post_shortcodes[] = rawurlencode( json_encode( $shortcode ) );
			
			
			$string .= $this->toString( $shortcode, $content );
		}
		
		return $string;
	}
	
	
	function getShortCode($tag){
		return Cl_Shortcode_Manager::getInstance()->setTag( $tag );
	}


	function toString( $shortcode, $content ) {
		$shortcode_obj = $this->getShortCode( $shortcode['tag'] );
		$is_container = $shortcode_obj->settings( 'is_container' ) || ( null !== $shortcode_obj->settings( 'as_parent' ) && false !== $shortcode_obj->settings( 'as_parent' ) );
		
		
		$output = ( '<div class="cl_element '.$shortcode_obj->getElementClass($shortcode['tag'])->getBackendClasses().'" data-tag="' . $shortcode['tag'] . '" data-shortcode-controls="' . esc_attr( json_encode( $shortcode_obj->getElementClass($shortcode['tag'])
		                                                                                                                                       ->getControlsList() ) ) . '" data-model-id="' . $shortcode['id'] . '">[' . $shortcode['tag'] . ' ' . $shortcode['attrs_query'] . ']' . ( $is_container ?  $this->parseShortcodesString( $content, $is_container, $shortcode['id'] ) : do_shortcode( $content ) ) . '[/' . $shortcode['tag'] . ']</div>' );
		                                                                                                                                   
		//$output = $shortcode['tag'];
		return $output;
	}
	
	function cl_test_do_shortcode( $content ){
		return;
	}

	function load_shortcode_ajax(){
		
		
		if(isset($_POST['action']) && $_POST['action'] == 'cl_load_shortcode'){
			
			Cl_Builder_Mapper::addShortcodes();
			$output = ob_start();
			$shortcodes = $_POST['shortcodes'];
			
			foreach ( $shortcodes as $shortcode ) {
				if ( isset( $shortcode['id'] ) && isset( $shortcode['string'] ) ) {
					if ( isset( $shortcode['tag'] ) ) {
						$shortcode_obj = $this->getShortCode( $shortcode['tag'] );
						if ( is_object( $shortcode_obj ) ) {
							
	//						$output .= '<div data-type="element" data-model-id="' . $shortcode['id'] . '">';
							$is_container = $shortcode_obj->settings( 'is_container' ) || ( null !== $shortcode_obj->settings( 'as_parent' ) && false !== $shortcode_obj->settings( 'as_parent' ) );
							
							echo '<div class="cl_element '.$shortcode_obj->getElementClass($shortcode['tag'])->getBackendClasses().'" data-shortcode-controls="' . esc_attr( json_encode( $shortcode_obj->getElementClass($shortcode['tag'])
			                                                                                                                                       ->getControlsList() ) ). '" data-model-id="' . $shortcode['id'] . '">';
			                echo	do_shortcode(  stripslashes($shortcode['string'])  ) ;
			                echo '</div>';
	//						$output .= '</div>';
						}
					}
				}
			}
			
			$output = ob_get_clean();
			die($output);
		}
		
	}
	
	function prepend_element_html(){

		?>
		
		<div class="add-element-prepend app-prepend"></div>

		
		<?php
	}
	
	function append_element_html(){
		
		if( ! is_page() && ! is_single() )
			return false;
			
		?>
		
		<div class="add-element-append app-append"></div>
		
		<?php
	}
	
	
}


?>