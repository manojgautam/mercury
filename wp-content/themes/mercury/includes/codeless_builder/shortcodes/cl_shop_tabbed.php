<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$output = '';

$atts = cl_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
                   
wp_reset_query();

// Element ID
$element_id = uniqid();

// Set Query

// Set some global vars for use with codeless_get_from_element or directly from cl_get_option
global $cl_from_element;
$cl_from_element['shop_product_style'] = $product_style;
$cl_from_element['shop_columns'] = 4;

/* Add Custom Styles */
wp_enqueue_style('codeless-woocommerce', CODELESS_BASE_URL.'css/codeless-woocommerce.css' );

if( isset( $_GET['use_for_ajax'] ) ){
	$operation = $_GET['use_for_ajax'];

	echo '<div class="used_for_ajax">';

		echo '<div class="tab">';
	
	if( $operation == 'featured' )
		echo do_shortcode( '[featured_products per_page="'.(int) $per_page.'" columns="4"]' );
	else if( $operation == 'top_sellers' )
		echo do_shortcode( '[best_selling_products per_page="'.(int) $per_page.'" columns="4"]' );
	else if( $operation == 'recent' )
		echo do_shortcode( '[recent_products per_page="'.(int) $per_page.'" columns="4"]' );

		echo '</div>';

	echo '</div>';

	exit();
}                     

// Start displaying WooCommerce Element                            
?>
<div id="<?php echo esc_attr( $element_id ) ?>" class="cl_shop_tabbed <?php echo esc_attr( $this->generateClasses('.cl_shop_tabbed') ) ?> cl-element" <?php $this->generateStyle('.cl_shop_tabbed', '', true) ?> >
	

	<ul class="tabbed-tabs">
		<li class="active"><a data-load="featured" href="#" class="h5"><?php esc_html_e( 'FEATURED', 'june' ) ?></a></li>
		<li><a data-load="recent" href="#" class="h5"><?php esc_html_e( 'NEW ARRIVALS', 'june' ) ?></a></li>
		<li><a data-load="top_sellers" href="#" class="h5"><?php esc_html_e( 'TOP SELLERS', 'june' ) ?></a></li>
	</ul>

	<div class="shop_tabbed_content">
		<div class="tab">
			<?php echo do_shortcode( '[featured_products per_page="'.$per_page.'" columns="4"]' ); ?>
		</div>
	</div>

</div><!-- .cl_woocommerce -->