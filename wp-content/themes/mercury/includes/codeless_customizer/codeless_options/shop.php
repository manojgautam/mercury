<?php

Kirki::add_section( 'cl_shop', array(
	    'priority'    => 10,
	    'type' => '',
	    'title'       => esc_html__( 'Shop', 'june' ),
	    'tooltip' => esc_html__( 'All Shop Options', 'june' ),
	) );
		
		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_product_style',
			'label'    => esc_html__( 'Shop Items Style', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'normal',
			'choices'     => array(
				'normal'  => esc_attr__( 'Normal', 'june' ),
				'small'  => esc_attr__( 'Small', 'june' ),
				'large'  => esc_attr__( 'Large', 'june' ),
				'masonry'  => esc_attr__( 'Masonry', 'june' ),
				'list'  => esc_attr__( 'List', 'june' )
			),
		) );

		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_advanced_list',
			'label'    => esc_html__( 'Advanced List', 'june' ),
			'tooltip' => esc_html__( 'Switch on to make List Style more Advanced', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'switch',
			'priority' => 10,
			'default'  => 0,
			'choices'     => array(
				1  => esc_attr__( 'On', 'june' ),
				0 => esc_attr__( 'Off', 'june' ),
			),
			'transport' => 'refresh',
			'required'    => array(
				array(
					'setting'  => 'shop_product_style',
					'operator' => '==',
					'value'    => 'list',
				),
								
			),
		) );

		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_columns',
			'label'    => esc_html__( 'Shop Columns', 'june' ),
			'tooltip' => esc_html__( 'Select number of items to display per Row on SHOP Page', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => '3',
			'choices'     => array(
				'2'  => esc_attr__( '2 Columns', 'june' ),
				'3'  => esc_attr__( '3 Columns', 'june' ),
				'4'  => esc_attr__( '4 Columns', 'june' ),
				'5'  => esc_attr__( '5 Columns', 'june' ),
				'6'  => esc_attr__( '6 Columns', 'june' ),
			),
		) );

		Kirki::add_field( 'cl_june', array(
		    'type' => 'slider',
		    'settings' => 'shop_item_distance',
			'label' => 'Distance between items',
			'default' => '15',
			'choices'     => array(
			'min'  => '0',
			'max'  => '30',
			'step' => '1',
							),
			'inline_control' => true,
			'section'  => 'cl_shop',
			'transport' => 'postMessage'
    	));

    	Kirki::add_field( 'cl_june', array(
				'settings' => 'shop_animation',
				'label'    => esc_html__( 'Animation Type', 'june' ),
				
				'section'  => 'cl_shop',
				'type'     => 'select',
				'priority' => 10,
				'default'  => 'bottom-t-top',
				'choices' => array(
					'none'	=> 'None',
					'top-t-bottom' =>	'Top-Bottom',
					'bottom-t-top' =>	'Bottom-Top',
					'right-t-left' => 'Right-Left',
					'left-t-right' => 'Left-Right',
					'alpha-anim' => 'Fade-In',	
					'zoom-in' => 'Zoom-In',	
					'zoom-out' => 'Zoom-Out',
					'zoom-reverse' => 'Zoom-Reverse',
				),
				'transport' => 'postMessage'
	) );

    	Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_item_heading',
			'label'    => esc_html__( 'Shop Item Heading', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'custom_font',
			'choices'     => array(
				'h1'  => esc_attr__( 'H1', 'june' ),
				'h2'  => esc_attr__( 'H2', 'june' ),
				'h3'  => esc_attr__( 'H3', 'june' ),
				'h4'  => esc_attr__( 'H4', 'june' ),
				'h5'  => esc_attr__( 'H5', 'june' ),
				'h6'  => esc_attr__( 'H6', 'june' ),
				'custom_font'  => esc_attr__( 'Custom Font', 'june' ),
			),
			'transport' => 'postMessage'
		) );


		Kirki::add_field( 'cl_june', array(
			'type'        => 'typography',
			'settings'    => 'shop_custom_typography',
			'label'       => esc_attr__( 'Shop Custom Title Font', 'june' ),
			'section'     => 'cl_shop',
			'into_group' => true,
			'show_subsets' => false,
			'show_variants' => true,
			'default'     => array(
				'letter-spacing' => '0.4px',
				'font-weight' => '600',
				'text-transform' => 'none',
				'font-size' => '14px',
				'line-height' => '26px',
				'color' => '#262a2c',
				'font-family' => 'Montserrat'
			),
			'priority'    => 10,
			'transport' => 'auto',
			'output'      => array(
				array(
					'element' => codeless_dynamic_css_register_tags( 'shop_custom_typography' )
				),

			),
			'required'    => array(
				array(
					'setting'  => 'shop_item_heading',
					'operator' => '==',
					'value'    => 'custom_font',
				),
								
			),
		));


    	Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_pagination_style',
			'label'    => esc_html__( 'Shop Pagination Style', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'load_more',
			'choices'     => array(
				'numbers'  => esc_attr__( 'With Page Numbers', 'june' ),
				'next_prev'  => esc_attr__( 'Next/Prev', 'june' ),
				'load_more'  => esc_attr__( 'Load More Button', 'june' ),
				'infinite_scroll'  => esc_attr__( 'Infinite Scroll', 'june' ),
			),
			'transport' => 'refresh'
		) );

		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_category_layout',
			'label'    => esc_html__( 'Shop Categories Layout', 'june' ),
			'tooltip' => esc_html__( 'Select shop Product Categories page layout.', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'left_sidebar',
			'choices'     => array(
				'fullwidth'  => esc_attr__( 'Fullwidth', 'june' ),
				'left_sidebar'  => esc_attr__( 'Left Sidebar', 'june' ),
				'right_sidebar'  => esc_attr__( 'Right Sidebar', 'june' )
			),
		) );

		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_inpage_filters',
			'label'    => esc_html__( 'Inpage Filters', 'june' ),
			'tooltip' => esc_html__( 'Filters on top of page will be shown. Please fill the Widgetized Area "Shop InPage Filters at Widgets." ', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'switch',
			'priority' => 10,
			'default'  => 0,
			'choices'     => array(
				1  => esc_attr__( 'On', 'june' ),
				0 => esc_attr__( 'Off', 'june' ),
			),
			'transport' => 'refresh',
		) );


		Kirki::add_field( 'cl_june', array(
			'settings' => 'shop_bottom_content',
			'label'    => esc_html__( 'Shop Bottom Content', 'june' ),
			'tooltip' => esc_html__( 'Select a page, use the content of that page as elements in the end of Shop Page', 'june' ),
			'section'  => 'cl_shop',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'none',
			'transport' => 'postMessage',
			'choices'  =>   codeless_get_pages()
		) );

		
?>