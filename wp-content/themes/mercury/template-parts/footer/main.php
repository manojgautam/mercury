<footer id="colophon" class="site-footer <?php echo esc_attr( codeless_extra_classes( 'site_footer' ) ) ?>">
    
    <div class="footer-content container-fluid">
        
        <div class="footer-content-row <?php echo esc_attr( codeless_extra_classes( 'footer_content_row' ) ) ?>">
            
             <?php codeless_build_footer_layout() ?>
            
        </div><!-- .footer-content-row -->
        
    </div><!-- .footer-content -->
 
</footer><!-- footer#colophon -->
<div id="copyright">
	
	<div class="copyright-content container-fluid">
        
        <div class="copyright-content-row <?php echo esc_attr( codeless_extra_classes( 'copyright_content_row' ) ) ?>">
            
             <?php codeless_build_copyright() ?>
            
        </div><!-- .copyright-content-row -->
        
    </div><!-- .copyright-content -->


</div>
