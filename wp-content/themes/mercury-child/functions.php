<?php

Function child_enqueue_scripts() {
  wp_register_style( 'childtheme_style', get_template_directory_uri() . '/style.css'  );
  wp_enqueue_style( 'childtheme_style' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts');
function wooc_extra_register_fields() {?>
       <p class="form-row form-row-wide">
       <label for="reg_billing_phone"><?php _e( 'SCHOOL', 'woocommerce' ); ?></label>
       <select name="school">
       <option value="">Please select your school</option>
    <option value="Frank Anthony Public School">Frank Anthony Public School</option>
    <option value="South Point School">South Point School</option>
  </select>
       </p>
      <div class="form-row form-row-wide">
       <label for="reg_billing_phone"><?php _e( 'School Provided Parent ID', 'woocommerce' ); ?></label>
       <input type="text" class="input-text" name="Parent_ID" id="Parent_ID" value="<?php esc_attr_e( $_POST['Parent_ID'] ); ?>" />
<div class="tooltip"><img src="/wp-content/themes/mercury-child/icon.png">
  <span class="tooltiptext">Parent ID is in the format e.g PG13/1234 or PB13/1234.</span>
</div>
       </div>
      <div class="form-row form-row-wide">
       <label for="dob"><?php _e( 'Child Date of Birth', 'woocommerce' ); ?></label>
       <input type="date" class="input-text" name="dob" id="dob" value="<?php esc_attr_e( $_POST['dob'] ); ?>" />
<div class="tooltip"><img src="/wp-content/themes/mercury-child/icon.png">
  <span class="tooltiptext">In case of more than one child, any child DOB may be entered.</span>
</div>
       </div>

      <div class="form-row form-row-wide">
       <label for="reg_billing_phone"><?php _e( 'Phone No', 'woocommerce' ); ?></label>
       <input type="number" class="input-text" name="billing_phone" id="billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" />
       </div>
      <div class="form-row form-row-wide">
       <label for="billing_phone_alternate"><?php _e( 'Alternate Contact Number', 'woocommerce' ); ?></label>
       <input type="number" class="input-text" name="billing_phone_alternate" id="billing_phone_alternate" value="<?php esc_attr_e( $_POST['billing_phone_alternate'] ); ?>" />
       </div>
      <div class="form-row form-row-wide">
       <label for="billing_address_1"><?php _e( 'Address', 'woocommerce' ); ?></label>
    <textarea name="billing_address_1" id="billing_address_1" cols="30" rows="10"></textarea>
<div class="tooltip"><img src="/wp-content/themes/mercury-child/icon.png">
  <span class="tooltiptext">Please provide complete address with Flat no, Tower no, Apartment no, etc</span>
</div>
       </div>
      <div class="form-row form-row-wide">
       <label for="billing_postcode"><?php _e( 'Pincode', 'woocommerce' ); ?></label>
       <input type="text" class="input-text" name="billing_postcode" id="billing_postcode" value="<?php esc_attr_e( $_POST['billing_postcode'] ); ?>" />
       </div>
      <div class="form-row form-row-wide">
       <label for="billing_landmark"><?php _e( 'Landmark', 'woocommerce' ); ?></label>
       <input type="text" class="input-text" name="billing_landmark" id="billing_city" value="<?php esc_attr_e( $_POST['billing_landmark'] ); ?>" /><div class="tooltip"><img src="/wp-content/themes/mercury-child/icon.png">
  <span class="tooltiptext">Please provide proper landmark for easy identification</span>
</div>

       </div>
       <div class="clear"></div>
       <?php
 }
 add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

function wooc_save_extra_register_fields( $customer_id ) {

       if ( isset( $_POST['school'] ) ) {
               update_user_meta( $customer_id, 'school', sanitize_text_field( $_POST['school'] ) );

       }

 

       if ( isset( $_POST['Parent_ID'] ) ) {

              update_user_meta( $customer_id, 'Parent_ID', sanitize_text_field( $_POST['Parent_ID'] ) );
       }

 

       if ( isset( $_POST['billing_phone'] ) ) {
            global $wpdb;
            $table_name="mcc_users";
            $phone=$_POST['billing_phone'];
              update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
            if($phone !=''){
              $wpdb->update($wpdb->users, array('user_login' => $phone), array('ID' => $customer_id));;
            }

       }
       if ( isset( $_POST['billing_phone_alternate'] ) ) {

              update_user_meta( $customer_id, 'billing_phone_alternate', sanitize_text_field( $_POST['billing_phone_alternate'] ) );

       }
       if ( isset( $_POST['billing_postcode'] ) ) {

              update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );

       }
       if ( isset( $_POST['billing_landmark'] ) ) {

              update_user_meta( $customer_id, 'billing_landmark', sanitize_text_field( $_POST['billing_landmark'] ) );

       }
       if ( isset( $_POST['dob'] ) ) {

              update_user_meta( $customer_id, 'dob', sanitize_text_field( $_POST['dob'] ) );

       }
       if ( isset( $_POST['billing_address_1'] ) ) {

              update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );

       }

}

 

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
 
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );

function my_woocommerce_edit_account_form() {

$user_id = get_current_user_id();
$user = get_userdata( $user_id );

if ( !$user )
  return;

$school = get_user_meta( $user_id, 'school', true );
$Parent_ID = get_user_meta( $user_id, 'Parent_ID', true );
$billing_phone = get_user_meta( $user_id, 'billing_phone', true );
$billing_phone_alternate = get_user_meta( $user_id, 'billing_phone_alternate', true );
$billing_address_1 = get_user_meta( $user_id, 'billing_address_1', true );
$billing_postcode = get_user_meta( $user_id, 'billing_postcode', true );
$billing_landmark = get_user_meta( $user_id, 'billing_landmark', true );
$dob = get_user_meta( $user_id, 'dob', true );
?>
<p class="form-row form-row-thirds">
       <label for="reg_billing_phone"><?php _e( 'SCHOOL NAME', 'woocommerce' ); ?></label>
       <select name="school">
       <option value="">Please select your school</option>
    <option value="Frank Anthony Public School" <?php if($school =="Frank Anthony Public School"){ echo "selected" ;}?>>Frank Anthony Public School</option>
    <option value="South Point School" <?php if($school =="South Point School"){ echo "selected" ;}?>>South Point School</option>
  </select>
</p>
<p class="form-row form-row-thirds">
  <label for="url">Parent ID:</label>
  <input type="text" name="Parent_ID" value="<?php echo esc_attr( $Parent_ID ); ?>" class="input-text" />
</p>
      <p class="form-row form-row-wide">
       <label for="dob"><?php _e( 'Childs Date of Birth', 'woocommerce' ); ?></label>
       <input type="date" class="input-text" name="dob" id="dob" value="<?php esc_attr_e( $dob ); ?>" />
       </p>
      <p class="form-row form-row-wide">
       <label for="reg_billing_phone"><?php _e( 'Phone No', 'woocommerce' ); ?></label>
       <input type="number" class="input-text" name="billing_phone" id="billing_phone" value="<?php esc_attr_e( $billing_phone ); ?>" />
       </p>
      <p class="form-row form-row-wide">
       <label for="billing_phone_alternate"><?php _e( 'Alternate Contact Number', 'woocommerce' ); ?></label>
       <input type="number" class="input-text" name="billing_phone_alternate" id="billing_phone_alternate" value="<?php esc_attr_e( $billing_phone_alternate ); ?>" />
       </p>
      <p class="form-row form-row-wide">
       <label for="billing_address_1"><?php _e( 'Address', 'woocommerce' ); ?></label>
    <textarea name="billing_address_1" id="billing_address_1" cols="30" rows="10"><?php esc_attr_e( $billing_address_1 ); ?></textarea>
       </p>
      <p class="form-row form-row-wide">
       <label for="billing_postcode"><?php _e( 'Pincode', 'woocommerce' ); ?></label>
       <input type="text" class="input-text" name="billing_postcode" id="billing_postcode" value="<?php esc_attr_e( $billing_postcode ); ?>" />
       </p>
      <p class="form-row form-row-wide">
       <label for="billing_landmark"><?php _e( 'Landmark', 'woocommerce' ); ?></label>
       <input type="text" class="input-text" name="billing_landmark" id="billing_landmark" value="<?php esc_attr_e( $billing_landmark ); ?>" />
       </p>
<?php
}

function my_woocommerce_save_account_details( $user_id ) {
global $wpdb;
  $table_name="mcc_users";
  $phone=$_POST['billing_phone'];
if($phone !=''){
  //$wpdb->query($wpdb->prepare("UPDATE $table_name SET user_login='$phone' WHERE ID=$user_id"));
  $wpdb->update($wpdb->users, array('user_login' => $phone), array('ID' => $user_id));
}
update_user_meta( $user_id, 'school', htmlentities( $_POST[ 'school' ] ) );
update_user_meta( $user_id, 'Parent_ID', htmlentities( $_POST[ 'Parent_ID' ] ) );
update_user_meta( $user_id, 'dob', htmlentities( $_POST[ 'dob' ] ) );
update_user_meta( $user_id, 'billing_phone', htmlentities( $_POST[ 'billing_phone' ] ) );
update_user_meta( $user_id, 'billing_phone_alternate', htmlentities( $_POST[ 'billing_phone_alternate' ] ) );
update_user_meta( $user_id, 'billing_address_1', htmlentities( $_POST[ 'billing_address_1' ] ) );
update_user_meta( $user_id, 'billing_postcode', htmlentities( $_POST[ 'billing_postcode' ] ) );
update_user_meta( $user_id, 'billing_landmark', htmlentities( $_POST[ 'billing_landmark' ] ) );

}
// regeneration restriction.
/*function wpse_131562_redirect() {
    if (
        ! is_user_logged_in()
        && (is_woocommerce() || is_cart() || is_checkout())
    ) {
        // feel free to customize the following line to suit your needs
        wp_redirect(home_url('my-account'));
        exit;
    }
}
add_action('template_redirect', 'wpse_131562_redirect');*/

//remove company name from checkout
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
function custom_override_checkout_fields( $fields ) {
unset($fields['billing']['billing_company']);
 
return $fields;
}

?>