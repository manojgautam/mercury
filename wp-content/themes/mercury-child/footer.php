<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #viewport #wrapper #main div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package june WordPress Theme
 * @subpackage Templates
 * @since 1.0
 */

?>
        <?php codeless_hook_main_end(); ?>
        
	    </main><!-- #main -->
        
        <?php
        
        /**
         * Functions hooked into codeless_hook_main_after action
         *
         * @hooked codeless_show_footer                     - 0
         */
        codeless_hook_main_after() ?>

	    <?php codeless_hook_wrapper_end() ?>
	    
    </div><!-- #wrapper -->
    
    <?php 

		/**
		 * Functions hooked into codeless_hook_wrapper_after action
		 *
		 * @hooked codeless_creative_search 				- 10 
		 */   
    	codeless_hook_wrapper_after()
    ?>

    <?php 

        /**
         * Functions hooked into codeless_hook_viewport_end action
         *
         * @hooked codeless_layout_bordered                 - 10 
         */ 
        codeless_hook_viewport_end() 

    ?>
    
</div><!-- #viewport -->

<?php codeless_hook_viewport_after() ?>
<?php
$url= $_SERVER['REQUEST_URI'];
$url=explode('/',$url);
$url=$url[2];
if($url=='frank-anthony-public-school'){ ?>
<style>
.cat-item-145 {
    display: none !important;
}
</style>
<?php

}elseif($url=='south-point-school'){?>
<style>
.cat-item-167 {
    display: none!important;
}
</style>
<?php

}
?>
<?php wp_footer(); ?>
<!--smooth scroll -->
<script>
jQuery('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
       jQuery('html, body').animate({
          scrollTop: target.offset().top - 72
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = jQuery(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });	
</script>

</body>
</html>