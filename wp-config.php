<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mercury_cellocraft');

/** MySQL database username */
define('DB_USER', 'agiledevelopers');

/** MySQL database password */
define('DB_PASSWORD', 'bRL7sTtFG-I@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(NL&NCmyfIdiN&qUTkn&M4)me!0cxL]z#4b5vWTCNQan9#7Goz>a2XK,$ *K[G[$');
define('SECURE_AUTH_KEY',  '>e7.!y>rJ:z:%`OP28sB]`3t,Mg*~Aqq4tX04/V94PVtr/;DH_jvxAoz)SYr/`bi');
define('LOGGED_IN_KEY',    'irtpb^aDPVtGuhVFREJ)@M5/{+RjZr>?lz _@w%&-|d{sl0Isw&aa7o$_484v$b{');
define('NONCE_KEY',        '{w_v:R4m^o~ZyQ;$nW5g7/;b~N  mBzen5_M qSXf*?rh+Pcy[eE&y%tl?`3ToYy');
define('AUTH_SALT',        'Te$y;pt)4SiW:gY<(i3Mp2j1@O1b^s!vpZMm;rb0&FUdaS+AN8/IlW50Ss6NYK]Y');
define('SECURE_AUTH_SALT', 'u~bSc,hOrKVf[QX}$ltIFgJxRWA2-[z<n6LXl3&g.n*%@1<d_bQ.:&nj0*>_+IKN');
define('LOGGED_IN_SALT',   'F8QrOs$7N.[$BOQUjm!./1TtZuicA2[ib0(64Jx^:g*0AkQX& *-q#xZ0/>Lo]1@');
define('NONCE_SALT',       'R&nhci]!~Vrvs.6C;gZaRGEd@p_djc<0>!<0X?,UbrnLUd5&r&{[K}a7|n]mY:zv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mcc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
